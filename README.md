# Assignment 6: Sign language translator

This application implements a sign language translator using react.js

## Storage
To store the data we use a json server deployed at https://assignment5-vue-dimitrov-loibl.herokuapp.com/

## Usage

The application is deployed at: https://obscure-woodland-51059.herokuapp.com/

If you want to run it yourself, you simpy need to clone the repo and run</br> ```npm install; npm run start```

## UI

Here you can see how the page looks
<details>
  <summary>home page</summary>
  <img src="screenshots/HomePage.jpg">
</details>
<details>
  <summary>profile page</summary>
  <img src="screenshots/ProfilePage.jpg">
</details>
<details>
  <summary>translation page</summary>
  <img src="screenshots/TranslationPage.jpg">
</details>


The component tree can be found <a href="./mockups/assignment_6_mockup.pdf" type=file>here</a> as pdf document

## Maintainer

[Petar Dimitrov]

[Philipp Loibl]

## License

[MIT]
---

[Petar Dimitrov]: https://github.com/PetarDimitrov91

[Philipp Loibl]: https://github.com/Loibl33

[MIT]: https://choosealicense.com/licenses/mit/


