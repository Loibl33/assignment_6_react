const host = 'https://assignment5-vue-dimitrov-loibl.herokuapp.com';
const apiKey = 'sdxtnepfrhgasecbtlaonljesdyplvwdziuygrfulbburyyevwousprvalhdmmrg';
async function getUserData(username) {
    try {
        const response = await fetch(host + `/translations?username=${username}`, {method: 'GET'});
        if(!response.ok){
            throw new Error('Fetching data not successful');
        }
        return response.json();
    } catch (e) {
        alert( + e.message);
    }
}

async function createUser(user) {
    try {
        const response = await fetch(host + `/translations`,
            {
                method: 'POST',
                headers: {
                    'X-API-Key': apiKey,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(user)
            });
        if(!response.ok){
            throw new Error('Creating not successful');
        }
        return response.json();
    }catch(e){
        alert( e.message);
    }
}

async function updateUserTranslations(id, translations) {
    try {
        const response = await fetch(host + `/translations/${id}`,
            {
                method: 'PATCH',
                headers: {
                    'X-API-Key': apiKey,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({translations: translations})
            });

        if(!response.ok){
            throw new Error('Updating not successful');
        }

        return response.json();
    }catch(e){
        alert( e.message);
    }
}

export {getUserData, createUser, updateUserTranslations};
