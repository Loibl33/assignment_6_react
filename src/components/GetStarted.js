import robo from '../assets/images/Logo-Hello.png'
import classes from './GetStarted.module.css'

function GetStarted() {

    return (
        <section className={classes.sectionOne}>
            <article>
                <img src={robo} className={classes.hello}/>
            </article>
            <article className={classes.text}>
                <h1 >Lost in Translation</h1>
                <h4>Get Started</h4>
            </article>
        </section>
    );
}

export default GetStarted;