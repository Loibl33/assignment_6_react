import arrow from '../assets/images/arrow.png';
import keyboard from '../assets/images/keyboard.ico';
import classes from './Input.module.css';
import {useContext, useRef} from 'react';

function Input(props) {
 let inputValue = '';
 let userRef = useRef();

    function getRef(){
     inputValue = userRef.current.value;
    }

    return (
        <form onSubmit={ (event) => {getRef(); event.preventDefault(); props.input(inputValue)}}>
            <img  src={keyboard}  className={classes.keyboard} />
            <input  type='text' placeholder={props.text} ref={userRef} />
          <button className={classes.btn} >  <img src={arrow} className={classes.arrow}  /></button>
        </form>
    );
}

export default Input;