import {Container} from '@mantine/core';
import classes from './Output.module.css';

function Output(props) {
    let word = []
    if (props.word !== undefined && props.word != '') {
        word = props.word.toLowerCase()
            .split(/\s+/)
            .reduce((crr, acc) => {
                return crr.concat(acc.split(""))
            }, []);
    }

    const renderSigns = word.map(l => (
        <img src={require(`../assets/signs/${l}.png`)}/>
    ));

    return (
        <Container size="xs" px="xs">
            <div className={classes.signsWrapper}>
                {renderSigns}
            </div>
        </Container>
    );
}

export default Output