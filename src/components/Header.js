import classes from './Header.module.css';
import logo from '../assets/images/Logo.png';
import profile from '../assets/images/profile.png';
import {Link} from 'react-router-dom';
import {useContext} from 'react';
import {UserContext} from '../store/UserContext';

function Header(props) {
    const [user, setUser] = useContext(UserContext);
    let hasUser = user !== undefined;

    return (
        <header className={classes.header}>

            <div className={classes.wrapper}>
                <img src={logo} className={classes.logo}/>
                <h1>Lost in Translation</h1>
            </div>

            {
                hasUser ?
                <nav className={classes.wrapper}>
                    <Link to="/translation" className={classes.btn}> Translate </Link>
                    <p>{props.username}</p>
                    <Link to="/profile"><img className={classes.profile} src={profile}/></Link>
                </nav>
                :
                ''
            }
        </header>
    );
}

export default Header;