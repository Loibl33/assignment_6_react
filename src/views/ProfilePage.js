import Header from '../components/Header';
import MainWrapper from '../components/MainWrapper';
import {useContext} from 'react';
import {UserContext} from '../store/UserContext';
import classes from './ProfilePage.module.css';

function TranslationPage(props) {
    const [user, setUser] = useContext(UserContext);

    const renderTranslations = user.translations
        .reverse()
        .map((t, index) => index < 10 && (
            <li>
                <p>Translation {index + 1}: <br/></p>
                <p>Text: {t} <br/> Signs: </p>
                {t.toLowerCase().split(/\s+/)
                    .reduce((acc, crr) => {
                        return acc.concat(crr.split(''))
                    }, [])
                    .map(l => (<img src={require(`../assets/signs/${l}.png`)}/>))}
            </li>
        ));

    return (
        <div>
            <Header username={user.username}/>
            <MainWrapper>
                <ul onClick={() => console.log(user)}>
                    {renderTranslations}
                </ul>
            </MainWrapper>
        </div>
    );
}

export default TranslationPage;