import Header from '../components/Header';
import MainWrapper from '../components/MainWrapper';
import GetStarted from '../components/GetStarted';
import Input from '../components/Input';
import classes from './HomePage.module.css'
import {createUser, getUserData} from '../services/UserService';
import {useContext, useRef} from 'react';
import {useHistory} from 'react-router-dom';
import {UserContext} from '../store/UserContext';

function HomePage() {
    const history = useHistory();
    let userObject;

    const [user, setUser] = useContext(UserContext);

    async function submitHandler(username) {
        if(username.length < 2){
            alert('Please enter a valid username: Min length: 2 characters')
          return;
        }

        let userdata = await getUserData(username);

        if (userdata.length === 0) {
            userObject = await createUser({username: username, translations: []});
        } else {
            userObject = userdata[0];
        }

        setUser(userObject);
        history.push('/translation');
    }

    return (
        <div>
            <Header/>
            <MainWrapper>
                <GetStarted/>
                <article className={classes.inputWrapper}>
                    <Input text='Enter your username' input={(username) => submitHandler(username)}/>
                </article>
            </MainWrapper>
            <div>
            </div>
        </div>
    )
}

export default HomePage