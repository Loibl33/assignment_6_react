import Header from '../components/Header';
import MainWrapper from '../components/MainWrapper';
import Input from '../components/Input';
import Output from '../components/Output';
import classes from './Translation.module.css'
import {useContext, useEffect, useState} from 'react';
import {UserContext} from '../store/UserContext';
import {updateUserTranslations} from '../services/UserService';

function TranslationPage(props) {
    const [user, setUser] = useContext(UserContext);

    let [word, setWord] = useState([''])

    useEffect(() => {
        let n = user.translations.length - 1
        word = user.translations[n];
    }, [word]);

    async function translateHandler(wordInput) {
        let word_san = wordInput.replace(/[^a-zA-Z\s+]+/g, '');

        if(!word_san){
            alert('Please enter valid word or sentence');
            return;
        }

        setWord(word_san);

        user.translations.push(word_san);
        setUser(user);

        await updateUserTranslations(user.id, user.translations);
    }

    return (
        <div>
            <Header username={user.username}/>
            <MainWrapper>

                <article className={classes.inputField}>
                    <Input text='translate' input={(word) => translateHandler(word)}/>
                </article>

            </MainWrapper>
            <div>
                <Output word={word}/>
            </div>
        </div>
    );
}

export default TranslationPage;