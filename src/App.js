import {BrowserRouter, Route, Switch} from 'react-router-dom'
import { GuardProvider, GuardedRoute } from 'react-router-guards';
import HomePage from './views/HomePage';
import TranslationPage from './views/TranslationPage';
import ProfilePage from './views/ProfilePage';
import {useContext} from 'react';
import {UserContext} from './store/UserContext';

function App() {
    const [user, setUser] = useContext(UserContext);

    const requireLogin = (to, from, next) => {
        if (to.meta.auth) {
            if (user !== undefined) {
                next();
            }
            next.redirect('/');
        } else {
            next();
        }
    };

    return (
        <BrowserRouter>
            <GuardProvider guards={[requireLogin]}>
                <Switch>
                    <Route path='/' exact component={HomePage}/>
                    <GuardedRoute path='/translation' exact component={TranslationPage} meta={{auth: true}}/>
                    <GuardedRoute path='/profile' component={ProfilePage}  meta={{auth: true}}/>
                </Switch>
            </GuardProvider>
        </BrowserRouter>
    )
}

export default App;